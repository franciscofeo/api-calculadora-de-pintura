
# 🔥 Code Challenge - Calculadora de Tinta

Esse repositório contém uma API que calcula a quantidade de tinta necessária para se pintar uma sala contendo 4 paredes. Ela está hospedada no _Heroku_, pode consultá-la clicando [aqui](https://api-tinta-calculadora.herokuapp.com/swagger-ui.html).

Os requisitos e as regras de negócio você pode consultar no repositório do desafio https://gitlab.com/digitalrepublic/code-challenge.

Além da API, foi criado um site simples apenas para facilitar o uso caso não queira utilizar ferramentas como o _Postman_ para gerar as requisições HTTP:

- [Repositório do site](https://github.com/franciscofeo/Site-Calculadora-Tinta)
- [Site](https://franciscofeo.github.io/Site-Calculadora-Tinta/)

## Tecnologias Utilizadas

- [Java](https://www.java.com/pt-BR/)
- [Spring Boot](https://spring.io/projects/spring-boot)
- [Spring Data JPA](https://spring.io/projects/spring-data-jpa)
- [H2](https://spring.io/projects/spring-data-jpa) 
- [Lombok](https://projectlombok.org/)
- [Swagger](https://swagger.io/)


## License

[![MIT License](https://img.shields.io/apm/l/atomic-design-ui.svg?)](https://github.com/tterb/atomic-design-ui/blob/master/LICENSEs)


## Rodando a Aplicação

Para iniciar, primeiro faça um clone do repositório utilizando o comando abaixo:

```bash
  git clone https://gitlab.com/franciscofeo/api-calculadora-de-pintura.git
```

Após isso, vá para a pasta raiz do repositório e agora você tem duas opções, iniciar a aplicação Spring com o Maven caso você já possua instalado na sua máquina, ou iniciar sem o Maven.

Se já possuir o maven instalado, utilize o seguinte comando:

```bash
    mvn spring-boot:run
```

Caso contrário, você pode iniciar a partir do arquivo _mvnw_, ou seja:

```bash
    mvnw spring-boot:run
```
## FAQ

#### Posso utilizar livremente a API?

Com certeza! O uso é livre, lembre-se de apenas dar os devidos créditos.

#### Encontrei algum bug, como entro em contato?

Você pode mandar um email para franciscoangelo.feo@gmail.com ou entrar em contato aqui mesmo pela plataforma.

