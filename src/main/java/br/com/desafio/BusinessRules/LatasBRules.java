package br.com.desafio.BusinessRules;


import br.com.desafio.Enum.Latas;
import java.util.*;
import java.util.stream.Collectors;


public class LatasBRules {


    public static String calcularGasto(int litrosTinta) {
        int litros = litrosTinta*1000;
        int qntddLitros;

        StringBuilder sb = new StringBuilder();

        // Recuperando o volume das latas que estão na entidade Enum
        List<Latas> latasEnum = new ArrayList<Latas>(Arrays.asList(Latas.values()));
        List<Integer> latas = latasEnum.stream().map(ls -> (ls.getValue()*1000)).map(Double::intValue).collect(Collectors.toList());
        // Passando o valor para mililitro pois é mais fácil de trabalhar e evita erros de arredondamento

        // Utilizar algum algoritmo de ordenação em ordem decrescente
        latas.sort(Collections.reverseOrder());
        for (Integer lata : latas) {
            if (litros >= lata) {
                qntddLitros = litros / lata;
                litros %= lata;

                String x = qntddLitros + " Latas de " + (double)lata/1000 + " Litros \n";

                sb.append(x);
                if (litros == 0) {
                    return sb.toString();
                }

            }
        }
        return sb.toString();
    }


}
