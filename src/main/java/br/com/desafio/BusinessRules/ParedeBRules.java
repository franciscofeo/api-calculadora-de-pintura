package br.com.desafio.BusinessRules;

import br.com.desafio.Model.Janela;
import br.com.desafio.Model.Parede;
import br.com.desafio.Model.Porta;

public class ParedeBRules {


    public static double totalAreaPJ(Parede parede) {
        return (parede.getQntddJanela()* Janela.getArea() + parede.getQntddPorta()* Porta.getArea());
    }

    public static double totalAreaParede(Parede parede){
        return parede.getAltura()*parede.getLargura();
    }

    public static int tintaNecessaria(Parede parede){
        double areaRemanescente = totalAreaParede(parede) - totalAreaPJ(parede);
        int litrosTinta = (int) Math.ceil(areaRemanescente / 5); // cada litro pinta 5 m^2
        System.out.println("Litros de tinta necessaria: " + litrosTinta);
        return litrosTinta;
    }

}
