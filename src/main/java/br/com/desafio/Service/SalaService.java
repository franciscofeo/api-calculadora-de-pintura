package br.com.desafio.Service;

import br.com.desafio.BusinessRules.LatasBRules;
import br.com.desafio.BusinessRules.ParedeBRules;
import br.com.desafio.Model.Parede;
import br.com.desafio.Model.Porta;
import br.com.desafio.Model.Sala;
import br.com.desafio.Repository.SalaRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Service
@Slf4j
public class SalaService {

    @Autowired
    public SalaRepository salaRepository;

    public ResponseEntity<String> save(Sala sala) {
        List<Parede> paredeList = new ArrayList<>(sala.getParedes());

        Stream<Parede> paredeAreaInvalida = paredeList.stream().filter(parede -> ParedeBRules.totalAreaParede(parede) > 15.0);
        Stream<Parede> areaPJInvalida = paredeList.stream().filter(parede ->  ParedeBRules.totalAreaPJ(parede) > 0.5*ParedeBRules.totalAreaParede(parede));
        Stream<Parede> alturaParedePorta = paredeList.stream().filter( parede -> {

            if(parede.getQntddPorta() != 0){
                return parede.getAltura() + 0.3 < Porta.altura;
            } else {
                return false;
            }
        });


        log.info("Condicional para saber se passou nas validações.");
        if(paredeAreaInvalida.findAny().isEmpty() && areaPJInvalida.findAny().isEmpty() && alturaParedePorta.findAny().isEmpty()){
            log.info("Dentro do condicional.");
            int litrosTinta = 0;
            for (Parede p : paredeList){
                litrosTinta += ParedeBRules.tintaNecessaria(p);
            }
            salaRepository.save(sala);
            log.info("Salvando a sala no banco de dados.");
            return new ResponseEntity<>(LatasBRules.calcularGasto(litrosTinta), HttpStatus.CREATED);
        }

        return ResponseEntity.badRequest().build();
    }

    public ResponseEntity<String> calcularGastoBD(Long id){
        Optional<Sala> salaOptional = salaRepository.findById(id);
        if(salaOptional.isPresent()){
            Sala sala = salaOptional.get();
            List<Parede> paredeList = new ArrayList<>(sala.getParedes());
            int litrosTinta = 0;
            for (Parede p : paredeList){
                litrosTinta += ParedeBRules.tintaNecessaria(p);
            }
            log.info(String.valueOf(litrosTinta));
            String resultado = LatasBRules.calcularGasto(litrosTinta);

            return ResponseEntity.ok(resultado);
        }
        return ResponseEntity.notFound().build();
    }

    public ResponseEntity<List<Sala>> findAll(){
        log.info("encontrar todos - metodo");
        return new ResponseEntity<>(salaRepository.findAll(), HttpStatus.OK);
    }


}

