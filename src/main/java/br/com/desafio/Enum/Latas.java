package br.com.desafio.Enum;

public enum Latas {
    LATA1(18.0), LATA2(3.6), LATA3(2.5), LATA4(0.5);

    private Double value;

    Latas(Double value) {
        this.value = value;
    }

    public Double getValue() {
        return value;
    }
}