package br.com.desafio.Controller;

import br.com.desafio.DTO.SalaDTO;
import br.com.desafio.Model.Sala;
import br.com.desafio.Service.SalaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class SalaController {

    @Autowired
    private SalaService salaService;

    @PostMapping
    public ResponseEntity<String> salvarParede(@RequestBody @Valid SalaDTO salaDTO){
        return salaService.save(salaDTO.dtoToSala());
    }

    @GetMapping
    public ResponseEntity<List<Sala>> mostrarSala(){
        return salaService.findAll();
    }
}
