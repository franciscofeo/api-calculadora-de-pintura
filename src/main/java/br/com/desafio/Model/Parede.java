package br.com.desafio.Model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Parede {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Schema(hidden = true)
    private Long id;

    @Min(1)
    private double altura;

    @Min(1)
    private double largura;

    private int qntddJanela;
    private int qntddPorta;

    @Column(name = "create_time", nullable = false, updatable = false)
    @CreationTimestamp
    @Schema(hidden = true)
    private LocalDateTime createDateTime;

    @Column(name = "update_time", nullable = false)
    @UpdateTimestamp
    @Schema(hidden = true)
    private LocalDateTime updateDateTime;

    public Parede(double altura, double largura, int qntddJanela, int qntddPorta) {
        this.altura = altura;
        this.largura = largura;
        this.qntddJanela = qntddJanela;
        this.qntddPorta = qntddPorta;
    }
}
