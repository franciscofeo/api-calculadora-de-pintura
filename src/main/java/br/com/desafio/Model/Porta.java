package br.com.desafio.Model;

public class Porta {

    public static double altura = 1.9;
    public static double largura = 0.8;

    // medidas porta  0,80 m x 1,90 m = 1,52 m^2
    public static double getArea() {
        return altura*largura;
    }
}
