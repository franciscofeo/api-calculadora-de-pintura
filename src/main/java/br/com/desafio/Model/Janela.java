package br.com.desafio.Model;

public class Janela {

    public static double altura = 2.00;
    public static double largura = 1.20;

    // medidas janela 2,00 m x 1,20 m = 2,4 m^2
    public static double getArea() {
        return altura*largura;
    }
}
