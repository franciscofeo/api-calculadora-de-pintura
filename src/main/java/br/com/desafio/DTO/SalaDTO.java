package br.com.desafio.DTO;

import br.com.desafio.Model.Parede;
import br.com.desafio.Model.Sala;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
public class SalaDTO {

    @Size(min = 4, max = 4, message = "A quantidade de paredes deve ser obrigatoriamente 4.")
    private List<Parede> paredes;

    public Sala dtoToSala(){
        return new Sala(paredes);
    }

}
